/*
** EPITECH PROJECT, 2019
** my.h
** File description:
** my
*/

#ifndef MY_H_
#define MY_H_
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
void gameloop(char **tab, int limit, int columns);
void printmap(char **tab);
#endif /* !MY_H_ */
