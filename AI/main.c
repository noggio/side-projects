/*
** EPITECH PROJECT, 2018
** main.c
** File description:
** main
*/

#include "my.h"

char **setmap(char **tab, int columns)
{
	int i;
	int k = 0;

	tab[0] = malloc(sizeof(char *) * 1000);
	for (i = 0; i < columns * 2 + 1; ++i)
		tab[0][i] = '*';
	tab[0][i+1] = '\0';
	for (int j = 1; j <= columns; j++) {
		tab[j] = malloc(sizeof(char *) * (columns * 2));
		for (int k = 0; k != j * 2 - 1; k++)
			tab[j][k] = '|';
	}
	return (tab);
}

void print_spaces(char **tab, int i)
{
	int spaces = (strlen(tab[0]) - strlen(tab[i]) - 2) / 2;

	for (int j = spaces; j > 0; j--)
		printf(" ");

}

void printmap(char **tab)
{
	printf("%s\n", tab[0]);
	for (int i = 1; tab[i]; i++) {
		printf("*");
		print_spaces(tab, i);
		printf("%s", tab[i]);
		print_spaces(tab, i);
		printf("*\n");
	}
	printf("%s\n", tab[0]);
}

void garbage_collector(char **tab)
{
	for (int i = 0; tab[i]; i++)
		free(tab[i]);
	free(tab);
}

int main(int ac, char **av)
{
	int columns;
	int limit;
	char **tab;

	if (ac < 3)
		exit (84);
	if (atoi(av[1]) > 0 && atoi(av[1]) < 100 && atoi(av[2]) > 0 &&
	atoi(av[2]) < 100) {
		columns = atoi(av[1]);
		limit = atoi(av[2]);
	} else
		exit(84);
	tab = malloc(sizeof(char **) * columns * 2 + 1);
	tab = setmap(tab, columns);
	gameloop(tab, limit, columns);
	//garbage_collector(tab);
}