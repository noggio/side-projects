/*
** EPITECH PROJECT, 2019
** gameloop.c
** File description:
** gameloop
*/

#include "my.h"

void remove_matches(int line, int matches, char **tab)
{
	int index = strlen(tab[line]);
	printf("%c\n", tab[line][index]);

	while (matches != 0 || index != 0) {
		//tab[line][index] = ' ';
		matches--;
		index--;
	}
}

void players_turn(char **tab, int limit, int columns)
{
	char *line = NULL;
	char *matches = NULL;
	size_t size = 0;

	printf("Your turn:\nLine:");
	getline(&line, &size, stdin);
	printf("Matches:");
	getline(&matches, &size, stdin);
	if (atoi(matches) > limit || atoi(line) > columns)
		exit (84);
	remove_matches(atoi(line), atoi(matches), tab);
}

void gameloop(char **tab, int limit, int columns)
{
	int win = 0;

	while (win != 1) {
		printmap(tab);
		players_turn(tab, limit, columns);
		win = 1;
	}
}