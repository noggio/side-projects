window.onload = function()
{
	var canvasWidth = 900;
	var canvasHeight = 600;
	var blockSize = 30;
	var ctx;
	var delay = 90;
	var snakey;
	var appley;
	var widthInBlocks = canvasWidth/blockSize;
	var heightInBlocks = canvasHeight/blockSize;
	var score;

	init();

	function init()
	{
		var canvas = document.createElement('canvas');
		canvas.width = canvasWidth;
		canvas.height = canvasHeight;
		canvas.style.border = "3px solid";
		document.body.appendChild(canvas);
		ctx = canvas.getContext("2d");
		snakey = new Snake([[6,4], [5,4], [4,4]], "right");
		appley = new Apple([10, 10]);
		score = 0;
		refreshCanvas();
	}
	function refreshCanvas()
	{
		snakey.advance();
		if (snakey.checkCollision())
		{
			gameOver();
		}
		else
		{
			if (snakey.isEating(appley))
			{
				score++;
				snakey.ateApple = true;
				do
				{
					appley.setNewPos();
				} while(appley.isOnSnake(snakey));
			}
			ctx.clearRect(0,0,canvasWidth, canvasHeight);
			snakey.draw();
			//snakey.drawEye1();
			//snakey.drawEye2();
			appley.draw();
			drawScore();
			setTimeout(refreshCanvas, delay);
		}
	}

	function gameOver()
	{
		ctx.save();
		ctx.fillText("Game Over", 5, 15);
		ctx.fillText("Appuyez sur Espace pour rejouer", 5, 30);
		ctx.restore();
	}

	function drawScore()
	{
		ctx.save();
		ctx.fillText(score.toString(), 5, canvasHeight - 5);
		ctx.restore();
	}

	function restart()
	{
		snakey = new Snake([[6,4], [5,4], [4,4]], "right");
		appley = new Apple([10, 10]);
		score = 0;
		refreshCanvas();
	}

	function drawBlock(ctx, position)
	{
		var x = position[0] * blockSize;
		var y = position[1] * blockSize;
		ctx.fillRect(x, y, blockSize, blockSize);
	}

	function Snake(body, direction)
	{
		this.body = body;
		this.direction = direction;
		this.ateApple = false;
		this.draw = function()
		{
			ctx.save();
			ctx.fillStyle = "#ff0000";
			for(var i = 0; i < this.body.length; i++) {
				drawBlock(ctx, this.body[i]);
			}
			ctx.restore();
		};
		this.advance = function()
		{
			var nextPos = this.body[0].slice();
			switch(this.direction) {
				case "left":
					nextPos[0] -= 1;
					break;
				case "right":
					nextPos[0] += 1;
					break;
				case "down":
					nextPos[1] += 1;
					break;
				case "up":
					nextPos[1] -= 1;
					break;
			}
			this.body.unshift(nextPos);
			if (!this.ateApple)
				this.body.pop();
			else
				this.ateApple = false;
		};
		this.setDirection = function(newDirection)
		{
			var allowedDirections;

			switch(this.direction)
			{
				case "left":
				case "right":
					allowedDirections = ["up", "down"];
					break;
				case "down":
				case "up":
					allowedDirections = ["left", "right"];
					break;
				default:
					throw("Invalid Direction");
			}
			if (allowedDirections.indexOf(newDirection) > - 1)
			{
				this.direction = newDirection;
			}
		};
		this.checkCollision = function()
		{
			var wallHit = false;
			var snakeHit = false;
			var head = this.body[0];
			var rest = this.body.slice(1);
			var snakeX = head[0];
			var snakeY = head[1];
			var minX = 0;
			var minY = 0;
			var maxX = widthInBlocks - 1;
			var maxY = heightInBlocks - 1;
			var isNotBetweenHWalls = snakeX < minX || snakeX > maxX;
			var isNotBetweenVWalls = snakeY < minY || snakeY > maxY;

			if (isNotBetweenHWalls || isNotBetweenVWalls)
			{
				wallHit = true;
			}
			for (var i = 0; i < rest.length; i++)
			{
				if(snakeX === rest[i][0] && snakeY === rest[i][1]) 
				{
					snakeHit = true;
				}
			}
			return wallHit || snakeHit;
		};
		this.isEating = function(appleToEat)
		{
			var head = this.body[0];
			if (head[0] === appleToEat.position[0] && head[1] === appleToEat.position[1])
				return true;
			else
				return false;
		};
		this.drawEye1 = function()
		{
			var head = this.body[0];
			var radius = blockSize / 4;
			var x = head[0] * blockSize + radius;
			var y = head[1] * blockSize + radius;
			ctx.save();
			ctx.fillStyle = "#fff"
			ctx.arc(x, y, radius, 0, Math.PI*2, true);
			ctx.fill();
			ctx.restore();
		};
		this.drawEye2 = function()
		{
			var head = this.body[0];
			var radius = blockSize / 4;
			var x = head[0] * blockSize + radius;
			var y = head[1] * blockSize + radius + 5;
			ctx.save();
			ctx.fillStyle = "#fff"
			ctx.arc(x, y, radius, 0, Math.PI*2, true);
			ctx.fill();
			ctx.restore();
		};
	}

	function Apple(position)
	{
		this.position = position;
		this.draw = function()
		{
			ctx.save();
			ctx.fillStyle = "#33cc33";
			ctx.beginPath();
			var radius = blockSize / 2;
			var x = this.position[0]*blockSize + radius;
			var y = this.position[1]*blockSize + radius;
			ctx.arc(x, y, radius, 0, Math.PI*2, true);
			ctx.fill();
			ctx.restore();
		};
		this.setNewPos = function()
		{
			var newX = Math.round(Math.random() * (widthInBlocks - 1));
			var newY = Math.round(Math.random() * (heightInBlocks - 1));
			this.position = [newX, newY];
		};
		this.isOnSnake = function(snakeToCheck)
		{
			var isOnSnake = false;
			
			for(var i = 0; i < snakeToCheck.body.length; i++)
			{
				if (this.position[0] === snakeToCheck.body[i][0] && this.position[1] === snakeToCheck.body[i][1])
					isOnSnake = true;
			}
			return isOnSnake;
		};
	}

	document.onkeydown = function handleKey(e)
		{
			var key = e.keyCode;
			var newDirection;
			switch(key)
			{
				case 37:
					newDirection = "left";
					break;
				case 38:
					newDirection = "up";
					break;
				case 39:
					newDirection = "right";
					break;
				case 40:
					newDirection = "down";
					break;
				case 32:
					restart();
					return;
				default:
					return;
			}
		snakey.setDirection(newDirection);
		}
}